const express = require('express');
const router = express.Router();

const importacionesModel = require("../models/importaciones");

router.get('/', function (req, res, next) {
    importacionesModel
        .obtener()
        .then(importaciones => {
            res.render("importaciones/ver", {
                importaciones: importaciones,
            });
        })
        .catch(err => {
            return res.status(500).send("Error obteniendo productos");
        });

});
router.get('/agregar', function (req, res, next) {
    res.render("importaciones/agregar");
});
router.post('/insertar', function (req, res, next) {
    // Obtener el nombre y precio. Es lo mismo que
    // const nombre = req.body.nombre;
    // const precio = req.body.precio;
    const { nombreI, codigoI, pais, proveedorI, iva, costotransporte, aduana, cantidadI, precioI } = req.body;
    if (!nombreI || !codigoI || !pais || !proveedorI || !iva || !costotransporte || !aduana || !cantidadI || !precioI) {
        return res.status(500).send("No hay dato");
    }
    // Si todo va bien, seguimos
    importacionesModel
        .insertar(nombreI, codigoI, pais, proveedorI, iva, costotransporte, aduana, cantidadI, precioI)
        .then(idProductoInsertado => {
            res.redirect("/importaciones");
        })
        .catch(err => {
            return res.status(500).send("Error insertando producto");
        });
});
router.get('/eliminar/:id', function (req, res, next) {
    importacionesModel
        .eliminar(req.params.id)
        .then(() => {
            res.redirect("/importaciones");
        })
        .catch(err => {
            return res.status(500).send("Error eliminando");
        });
});
router.get('/editar/:id', function (req, res, next) {
    importacionesModel
        .obtenerPorId(req.params.id)
        .then(importaciones => {
            if (importaciones) {
                res.render("importaciones/editar", {
                    importaciones: importaciones,
                });
            } else {
                return res.status(500).send("No existe producto con ese id");
            }
        })
        .catch(err => {
            return res.status(500).send("Error obteniendo producto");
        });
});
router.post('/actualizar/', function (req, res, next) {
    // Obtener el nombre y precio. Es lo mismo que
    // const nombre = req.body.nombre;
    // const precio = req.body.precio;
    const { id, nombreI, codigoI, pais, proveedorI, iva, costotransporte, aduana, cantidadI, precioI } = req.body;
    if (!nombreI || !codigoI || !pais || !proveedorI || !iva || !costotransporte || !aduana || !cantidadI || !precioI || !id) {
        return res.status(500).send("No hay suficientes datos");
    }
    // Si todo va bien, seguimos
    importacionesModel
        .actualizar(id, nombreI, codigoI, pais, proveedorI, iva, costotransporte, aduana, cantidadI, precioI)
        .then(() => {
            res.redirect("/importaciones");
        })
        .catch(err => {
            return res.status(500).send("Error actualizando producto");
        });
});

module.exports = router;

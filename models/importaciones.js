const conexion = require("../conexion")
module.exports = {
    insertar(nombreI, codigoI, pais, proveedorI, iva, costotransporte, aduana, cantidadI, precioI) {
        return new Promise((resolve, reject) => {
            conexion.query(`insert into productosimportados
            (nombreI, codigoI, pais, proveedorI, iva, costotransporte, aduana, cantidadI, precioI)
            values
            (?, ?, ?, ?, ?, ?, ?, ?, ?)`,
                [nombreI, codigoI, pais, proveedorI, iva, costotransporte, aduana, cantidadI, precioI], (err, resultados) => {
                    if (err) reject(err);
                    else resolve(resultados.insertId);
                });
        });
    },
    obtener() {
        return new Promise((resolve, reject) => {
            conexion.query(`select id, nombreI, codigoI, pais, proveedorI, iva, costotransporte, aduana, cantidadI, precioI from productosimportados`,
                (err, resultados) => {
                    if (err) reject(err);
                    else resolve(resultados);
                });
        });
    },
    obtenerPorId(id) {
        return new Promise((resolve, reject) => {
            conexion.query(`select id, nombreI, codigoI, pais, proveedorI, iva, costotransporte, aduana, cantidadI, precioI from productosimportados where id = ?`,
                [id],
                (err, resultados) => {
                    if (err) reject(err);
                    else resolve(resultados[0]);
                });
        });
    },
    actualizar(id, nombreI, codigoI, pais, proveedorI, iva, costotransporte, aduana, cantidadI, precioI) {
        return new Promise((resolve, reject) => {
            conexion.query(`update productosimportados
            set nombreI = ?,
            codigoI = ?,
            pais = ?,
            proveedorI = ?,
            iva = ?,
            costotransporte = ?,
            aduana = ?,
            cantidadI = ?,
            precioI = ?
            where id = ?`,
                [nombreI, codigoI, pais, proveedorI, iva, costotransporte, aduana, cantidadI, precioI, id],
                (err) => {
                    if (err) reject(err);
                    else resolve();
                });
        });
    },
    eliminar(id) {
        return new Promise((resolve, reject) => {
            conexion.query(`delete from productosimportados
            where id = ?`,
                [id],
                (err) => {
                    if (err) reject(err);
                    else resolve();
                });
        });
    },
}
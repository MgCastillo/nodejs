create database compras;
create table productos(
    id integer not null auto_increment,
    nombre varchar(255),
    codigo int,
    proveedor VARCHAR(255),
    cantidad int,
    precio decimal(5, 2),
    primary key(id)
);

create table productosImportados(
    id integer not null auto_increment,
    nombreI varchar(255),
    codigoI int,
    pais VARCHAR(255),
    proveedorI VARCHAR(255),
    iva VARCHAR(100),
    costotransporte int,
    aduana varchar(255),
    cantidadI int,
    precioI decimal(5, 2),
    primary key(id)
);